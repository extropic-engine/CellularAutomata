#include <SDL/SDL.h>
#include "timer.h"

Timer::Timer() {
    startTicks = 0;
    started = false;
    fpsCap = 60;
}

void Timer::startFrame() {
    started = true;
    startTicks = SDL_GetTicks();
}

void Timer::endFrame() const {
    if ( getTicks() < 1000 / fpsCap ) { 
        SDL_Delay((1000 / fpsCap) - getTicks()); 
    }
}

int Timer::getTicks() const {
    if (started == true) {
        return SDL_GetTicks() - startTicks;
    }

    return 0;
}
