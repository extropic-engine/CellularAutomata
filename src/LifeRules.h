#ifndef LIFE_RULES_H
#define LIFE_RULES_H

#include <memory>
#include <vector>

#include "Game.h"
#include "types.h"

struct Rule {
	Rule(const int ct, const int nt, const int nc, const int tt) {
		cell_type = ct; neighbor_type = nt; neighbor_count = nc; transition_to = tt;
	}
	int cell_type;      // The cell type that this rule applies to.
	int neighbor_type;	// The type of neighbors that this rule counts
	int neighbor_count; // The number of neighbors required for the rule to take effect
	int transition_to;  // The type that this cell transitions to when this rule takes effect.
};

class LifeRules: public Game {
public:
    LifeRules(SDL_Surface* _screen, int _x, int _y);
    void reset() override;
    void event(SDL_Event event) override;
    void update() override;
    void draw() override;
protected:
	~LifeRules() = default;
private:
    int x, y;
    std::shared_ptr<std::vector<std::vector<int>>> grid;
    std::shared_ptr<std::vector<std::vector<int>>> swapgrid;
    std::vector<u32> colors;
    std::vector<Rule> rules;
};

#endif