#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>

#include "Game.h"

Game::Game(SDL_Surface* _screen, int x, int y) {
    screen = _screen;
    screen_x = x;
    screen_y = y;
    quit = false;
}
