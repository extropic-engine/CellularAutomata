#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>
#include <iostream>

#include "Menu.h"
#include "LifeGame.h"
#include "LifeRules.h"
#include "Viruses.h"

using namespace std;

Menu::Menu(const int _x, const int _y) : Game(nullptr, _x, _y) {
    screen_x = _x;
    screen_y = _y;

    screen = SDL_SetVideoMode(screen_x, screen_y, 32, SDL_HWSURFACE|SDL_DOUBLEBUF);

    if (screen == nullptr) {
        fprintf(stderr, "Unable to set %dx%d video: %s\n", screen_x, screen_y, SDL_GetError());
        SDL_Quit();
    }

    SDL_WM_SetCaption("Cellular Automata", nullptr);
    current_selection = 0;
    menu_list.emplace_back("Conway's Game of Life");
    menu_list.emplace_back("Generic Cellular Automata");
    menu_list.emplace_back("Viruses & Vaccines");
    game_list.push_back(make_shared<LifeGame>(screen, screen_x, screen_y));
    game_list.push_back(make_shared<LifeRules>(screen, screen_x, screen_y));
    game_list.push_back(make_shared<Viruses>(screen, screen_x, screen_y));
    font = TTF_OpenFont("pcsenior.ttf", 14);
}

Menu::~Menu() {
    TTF_CloseFont(font);
}

void Menu::reset() {
    SDL_WM_SetCaption("Cellular Automata", nullptr);
    current_selection = 0;
}

void Menu::update() {
    if (current_game != shared_ptr<Game>()) {
        current_game->update();
    }
}

void Menu::event(SDL_Event event) {
    if (current_game != shared_ptr<Game>()) {
        current_game->event(event);
        if (current_game->quit) {
            current_game = shared_ptr<Game>();
            this->reset();
        }
        return;
    }

    if (event.type == SDL_KEYDOWN) {
        if (event.key.keysym.sym == SDLK_DOWN) {
            if (current_selection < menu_list.size() - 1) {
                current_selection++;
            }
        } else if (event.key.keysym.sym == SDLK_UP) {
            if (current_selection > 0) {
                current_selection--;
            }
        } else if (event.key.keysym.sym == SDLK_SPACE) {
            current_game = game_list[current_selection];
            current_game->reset();
        }
    }
}

void Menu::draw() {
    if (current_game != nullptr) {
        current_game->draw();
        return;
    }

    const float cell_height = screen_y / menu_list.size();
    constexpr SDL_Color black = {0,0,0};
    for (int i = 0; i < menu_list.size(); i++) {
        SDL_Rect rect;
        rect.x = 0;
        rect.y = i * cell_height;
        rect.w = screen_x;
        rect.h = (i+1) * cell_height;
        if (current_selection == i) {
            SDL_FillRect(screen, &rect, SDL_MapRGB(screen->format, 255, 0, 0));
        } else {
            SDL_FillRect(screen, &rect, SDL_MapRGB(screen->format, 255, 255, 255));
        }
        int w,h;
        TTF_SizeText(font, menu_list[i].c_str(),&w,&h);
        rect.x = (screen_x / 2) - (w/2);
        rect.y = (i * cell_height) + (cell_height / 2) - (h/2);
        SDL_Surface* text = TTF_RenderText_Solid(font, menu_list[i].c_str(), black);
        SDL_BlitSurface(text, nullptr, screen, &rect);
        SDL_FreeSurface(text);
    }
    SDL_Flip(screen);
}
