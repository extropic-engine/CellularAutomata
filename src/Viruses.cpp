#include <SDL/SDL.h>
#include <iostream>
#include <stdlib.h>

#include "Viruses.h"

using namespace std;

Viruses::Viruses(SDL_Surface* _screen, int _x, int _y) : Game(_screen, _x, _y) {
    x = screen_x / 4;
    y = screen_y / 4;
    grid.reset(new vector<vector<int> >);
    swapgrid.reset(new vector<vector<int> >);
    colors.push_back(SDL_MapRGB(screen->format, 0,0,0));
    colors.push_back(SDL_MapRGB(screen->format, 255, 255, 255));
    colors.push_back(SDL_MapRGB(screen->format, 255,0,255));
    colors.push_back(SDL_MapRGB(screen->format, 0, 255, 255));
    this->reset();
}

void Viruses::reset() {
    SDL_WM_SetCaption("Viruses & Vaccines", nullptr);
    grid->resize(x);
    for (int i = 0; i < x; i++) {
        (*grid)[i].resize(y);
        for (int m = 0; m < y; m++) {
            (*grid)[i][m] = rand() % 4;
        }
    }

    swapgrid->resize(x);
    for (int i = 0; i < x; i++) {
        (*swapgrid)[i].resize(y);
        for (int m = 0; m < y; m++) {
            (*swapgrid)[i][m] = 0;
        }
    }
}

void Viruses::event(SDL_Event event) {
    if (event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_ESCAPE) {
        quit = true;
    }
}

void Viruses::update() {
    for (int r = 0; r < x; r++) {
        for (int c = 0; c < y; c++) {
            
            // count neighbors
            int neighbors = 0;
            for (int i = r - 1; i <= r + 1; i++) {
                for (int m = c - 1; m <= c + 1; m++) {
                    if (i >= 0 && i < x && m >= 0 && m < y) {                        
                        if ((*grid)[i][m] >= 1) {
                            if (!(i == r && m == c)) {
                                neighbors++;
                            }
                        }
                    }
                }
            }
            
            // apply rules
            if ((*grid)[r][c] == 0 && neighbors == 3) {
                (*swapgrid)[r][c] = (rand() % 3) + 1;
            } else if ((*grid)[r][c] >= 1 && neighbors >= 2 && neighbors <= 3) {
                (*swapgrid)[r][c] = (rand() % 3) + 1;
            } else {
                (*swapgrid)[r][c] = 0;
            }
        }
    }
    
    shared_ptr<vector<vector<int> > > temp;
    temp = grid;
    grid = swapgrid;
    swapgrid = temp;
}

void Viruses::draw() {
    const float cell_width = screen_x / x;
    const float cell_height = screen_y / y;
    for (int r = 0; r < x; r++) {
        for (int c = 0; c < y; c++) {
            SDL_Rect rect;
            rect.x = r * cell_width;
            rect.y = c * cell_height;
            rect.w = (r+1) * cell_width;
            rect.h = (c+1) * cell_height;
            SDL_FillRect(screen, &rect, colors[(*grid)[r][c]]);
        }
    }
    
    SDL_Flip(screen);
}
