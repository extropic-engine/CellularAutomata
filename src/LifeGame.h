#ifndef LIFE_GAME_H
#define LIFE_GAME_H

#include <memory>
#include <vector>

#include "Game.h"
#include "types.h"

class LifeGame: public Game {
public:
    LifeGame(SDL_Surface* _screen, int _x, int _y);
    void reset() override;
    void event(SDL_Event event) override;
    void update() override;
    void draw() override;
protected:
    ~LifeGame() = default;
private:
    int x, y;
    std::shared_ptr<std::vector<std::vector<int>>> grid;
    std::shared_ptr<std::vector<std::vector<int>>> swapgrid;
    std::vector<u32> colors;
};

#endif