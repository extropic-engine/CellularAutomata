#ifndef VIRUSES_H
#define VIRUSES_H

#include <memory>
#include <vector>

#include "Game.h"

class Viruses: public Game {
public:
    Viruses(SDL_Surface* _screen, int _x, int _y);
    void reset();
    void event(SDL_Event event);
    void update();
    void draw();
private:
    int x, y;
    std::shared_ptr<std::vector<std::vector<int>>> grid;
    std::shared_ptr<std::vector<std::vector<int>>> swapgrid;
    std::vector<u32> colors;
};

#endif
