#ifndef MENU_H
#define MENU_H

#include <memory>
#include <vector>
#include <string>
#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>

#include "Game.h"

class Menu: public Game {
public:
    Menu(int _x, int _y);
    void reset() override;
    void event(SDL_Event event) override;
    void update() override;
    void draw() override;
protected:
	~Menu();
private:
	int current_selection;
	std::vector<std::string> menu_list;
	std::vector<std::shared_ptr<Game>> game_list;
	std::shared_ptr<Game> current_game;
	TTF_Font* font;
};

#endif