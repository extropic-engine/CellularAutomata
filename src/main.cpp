#include <SDL/SDL.h>
#include <iostream>

#include "timer.h"
#include "Menu.h"
#include "Viruses.h"

using namespace std;

int main(int argc, char* argv[]) {

    // ************************************** SDL INITIALIZATION **********************************
    if ( SDL_Init( SDL_INIT_EVERYTHING ) < 0 ) {
        fprintf(stderr, "Unable to init SDL: %s\n", SDL_GetError() );
        SDL_Quit();
    }
    atexit(SDL_Quit);

    if (TTF_Init() < 0) {
        fprintf(stderr, "Unable to init SDL_ttf: %s\n", SDL_GetError());
        TTF_Quit();
    }
    atexit(TTF_Quit);
    // ************************************** SDL INITIALIZATION **********************************

    SDL_Event event;
    bool quit = false;
    Timer timer;
    
    Menu menu(800, 600);

    srand(time(nullptr));

    menu.update();
    menu.draw();

    while (quit == false) {
        timer.startFrame();

        while (SDL_PollEvent(&event)) {
            if (event.type == SDL_QUIT) { 
                quit = true;
            } else if (event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_ESCAPE) {
                quit = true;
            }
            menu.event(event);
        }

        menu.update();
        menu.draw();

        cout << "Frame rendered in " << timer.getTicks() << "ms." << endl;
        timer.endFrame();
    }

    return 0;
}