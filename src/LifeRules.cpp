#include <SDL/SDL.h>
#include <iostream>
#include <cstdlib>

#include "LifeRules.h"

using namespace std;

LifeRules::LifeRules(SDL_Surface* _screen, int _x, int _y) : Game(_screen, _x, _y) {
    x = screen_x / 4;
    y = screen_y / 4;
    grid.reset(new vector<vector<int>>);
    swapgrid.reset(new vector<vector<int>>);
    colors.push_back(SDL_MapRGB(screen->format, 0,0,0));
    colors.push_back(SDL_MapRGB(screen->format, 255, 255, 255));
    rules.push_back(Rule(0, 1, 3, 1)); // dead cell comes alive when surrounded by 3 live cells
    rules.push_back(Rule(1, 1, 0, 0)); // live cells die when lonely
    rules.push_back(Rule(1, 1, 1, 0)); // live cells die when lonely
    rules.push_back(Rule(1, 1, 4, 0)); // live cells die when crowded
    rules.push_back(Rule(1, 1, 5, 0)); // live cells die when crowded
    rules.push_back(Rule(1, 1, 6, 0)); // live cells die when crowded
    rules.push_back(Rule(1, 1, 7, 0)); // live cells die when crowded
    rules.push_back(Rule(1, 1, 8, 0)); // live cells die when crowded
    this->reset();
}

void LifeRules::reset() {
    SDL_WM_SetCaption("Conway's Game of Life", nullptr);
    grid->resize(x);
    for (int i = 0; i < x; i++) {
        (*grid)[i].resize(y);
        for (int m = 0; m < y; m++) {
            (*grid)[i][m] = rand() % 2;
        }
    }

    swapgrid->resize(x);
    for (int i = 0; i < x; i++) {
        (*swapgrid)[i].resize(y);
        for (int m = 0; m < y; m++) {
            (*swapgrid)[i][m] = 0;
        }
    }
}

void LifeRules::event(const SDL_Event event) {
    if (event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_ESCAPE) {
        quit = true;
    }
}

void LifeRules::update() {
    for (int r = 0; r < x; r++) {
        for (int c = 0; c < y; c++) {
            
            // count neighbors
            int neighbors = 0;
            for (int i = r - 1; i <= r + 1; i++) {
                for (int m = c - 1; m <= c + 1; m++) {
                    if (i >= 0 && i < x && m >= 0 && m < y) {                        
                        if ((*grid)[i][m] == 1) {
                            if (!(i == r && m == c)) {
                                neighbors++;
                            }
                        }
                    }
                }
            }
            

            // TODO: read more than one type of neighbor
            bool rule_applied = false;
            for (const Rule &rule: rules) {
                if ((*grid)[r][c] == rule.cell_type) {
                    if (neighbors == rule.neighbor_count) {
                        (*swapgrid)[r][c] = rule.transition_to;
                        rule_applied = true;
                    }
                }
            }

            if (!rule_applied) {
                (*swapgrid)[r][c] = (*grid)[r][c];
            }
        }
    }

    const shared_ptr<vector<vector<int> > > temp = grid;
    grid = swapgrid;
    swapgrid = temp;
}

void LifeRules::draw() {
    const f32 cell_width = screen_x / x;
    const f32 cell_height = screen_y / y;
    for (u32 r = 0; r < x; r++) {
        for (u32 c = 0; c < y; c++) {
            SDL_Rect rect;
            rect.x = r * cell_width;
            rect.y = c*cell_height;
            rect.w = (r+1) * cell_width;
            rect.h = (c+1) * cell_height;
            SDL_FillRect(screen, &rect, colors[(*grid)[r][c]]);
        }
    }
    
    SDL_Flip(screen);
}
