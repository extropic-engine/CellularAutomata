#ifndef TIMER_H
#define TIMER_H

class Timer {
public:
    Timer();

    void startFrame();
    void endFrame() const;

    int getTicks() const;

    /*
    int setFPSCap();
    int getFPSCap();
    int getAverageFPS();
    */

private:
    bool started;
    int startTicks;
    int fpsCap;
};

#endif

