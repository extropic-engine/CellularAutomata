#ifndef GAME_H
#define GAME_H

class Game {
public:
    Game(SDL_Surface* _screen, int x, int y);

    virtual void reset() = 0;
    virtual void event(SDL_Event event) = 0;
    virtual void update() = 0;
    virtual void draw() = 0;
    bool quit;
protected:
    ~Game() = default;

    int screen_x, screen_y;
    SDL_Surface* screen;
};

#endif