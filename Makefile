Compiler     := clang++
Kernel       := $(shell uname -s)
Distro       := $(shell uname -r)
Target       := cellular-automata
TestTarget   := test
SourceDirs   := $(shell ls src/*.cpp)
Objects      := $(patsubst %.cpp, %.o, $(SourceDirs))
BinPath      := .
Libraries    := -lSDL -lSDL_ttf
Includes     :=
LinkFlags    := -g -Wall -O0 -std=c++11
CompileFlags :=

# Mac OS X specific settings
ifeq ($(Kernel), Darwin)
Libraries    += lSDLMain
LinkFlags    += -stdlib=libc++
CompileFlags += -framework Cocoa
endif

# target specific settings
test: Libraries += -lboost_unit_test_framework-mt
test: Sources   += tst/*.cpp

all: $(Objects)
	$(warning Building...)
	$(Compiler) $(Libraries) $(Objects) $(Sources) $(Includes) -o $(BinPath)/$(Target) $(LinkDirs) $(LinkFlags) $(CompileFlags)

.cpp.o:
	$(Compiler) $(LinkFlags) $(Includes) -c -o $*.o $<

.cxx.o:
	$(Compiler) $(LinkFlags) $(Includes) -c -o $*.o $<

test:
	$(warning Building Test Suite...)
	@$(Compiler) $(Libraries) $(Sources) $(Includes) -o $(BinPath)/$(TestTarget) $(LinkDirs) $(LinkFlags) $(CompileFlags)
	@-$(BinPath)/$(TestTarget) --log-level=all
	@rm $(BinPath)/$(TestTarget)

clean:
	rm -f $(Objects)

#multilib handling
ifeq ($(HOSTTYPE), x86_64)
LIBSELECT=64
endif
