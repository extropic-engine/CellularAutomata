Cellular-Automata
=================

A cellular automata simulator written with SDL.

[![Build Status](https://travis-ci.org/extropic-engine/Cellular-Automata.png?branch=master)](https://travis-ci.org/extropic-engine/Cellular-Automata)